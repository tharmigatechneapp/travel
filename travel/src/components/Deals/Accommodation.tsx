import {
  Box,
  Container,
  Grid,
  List,
  Typography,
  ListItem,
  CardContent,
  Paper,
} from "@mui/material";
import React from "react";
import Image from "next/image";

// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

// import "./styles.css";

// import required modules
import { Pagination } from "swiper";
import Popup from "./HotelPopup";
const Accommodation = () => {
  return (
    <Box
      sx={{
        bgcolor: "#D6E8F9",
        pb: 3,
      }}
    >
      <Container>
        <Typography
          sx={{
            textAlign: "left",
            fontFamily: "Chonburi",
            color: "#1B0B7D",

            p: 2,

            fontSize: { md: "1.7rem", xs: "1rem", sm: "1.5rem" },
          }}
        >
          Accommodation
        </Typography>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6} md={6}>
            <Typography
              className="textOverview"
              variant="body2"
              sx={{
                // p: 2,

                fontSize: { md: "0.9rem", xs: "0.8rem", sm: "0.8rem" },
                alignItems: "left",
              }}
            >
              <Box>
                <ul>
                  <li>Choose from Standard or Premium Accommodation</li>
                  <li>8 nights hotel accommodation on tour in Sri Lanka</li>
                  <li>
                    4 nights at the 4-star Adaaran Club Rannalhi, Maldives
                  </li>
                  <li>8 nights hotel accommodation on tour in Sri Lanka</li>
                  <li>
                    4 nights at the 4-star Adaaran Club Rannalhi, Maldives
                  </li>
                </ul>
              </Box>
            </Typography>
          </Grid>

          <Grid item md={6} xs={12} sm={12}>
            <Box>
              <Swiper
                slidesPerView={2}
                spaceBetween={0}
                pagination={{
                  clickable: true,
                }}
                modules={[Pagination]}
                className="mySwiper"
              >
                <SwiperSlide>
                  <Box sx={{ boxShadow: 5, borderRadius: 2, m: "1rem" }}>
                    <Image
                      className="img"
                      src="/images/hotel1.jpg"
                      alt="header"
                      height="215"
                      width="335"
                    />
                    <CardContent sx={{ py: "0.5rem" }}>
                      <Typography
                        // className="txt"
                        variant="body2"
                        className="text"
                        sx={{
                          fontSize: {
                            md: "0.7rem",
                            xs: "0.6rem",
                            sm: "0.6rem",
                          },
                          fontWeight: "bold",
                        }}
                      >
                        The Lucky Elepahant Hotel in Hikkaduwa
                      </Typography>
                      <Popup />
                    </CardContent>
                  </Box>
                </SwiperSlide>

                <SwiperSlide>
                  <Box sx={{ boxShadow: 5, borderRadius: 2, m: "1rem" }}>
                    <Image
                      className="img"
                      src="/images/hotel2.jpg"
                      alt="header"
                      height="215"
                      width="335"
                    />
                    <CardContent sx={{ py: "0.5rem" }}>
                      <Typography
                        // className="txt"
                        variant="body2"
                        className="text"
                        sx={{
                          fontSize: {
                            md: "0.7rem",
                            xs: "0.6rem",
                            sm: "0.6rem",
                          },
                          fontWeight: "bold",
                        }}
                      >
                        Zest Metropole Hotel in Fort, Colombo
                      </Typography>
                      <Popup />
                    </CardContent>
                  </Box>
                </SwiperSlide>

                <SwiperSlide>
                  <Box sx={{ boxShadow: 5, borderRadius: 2, m: "1rem" }}>
                    <Image
                      className="img"
                      src="/images/hotel3.jpg"
                      alt="header"
                      height="215"
                      width="335"
                    />
                    <CardContent sx={{ py: "0.5rem" }}>
                      <Typography
                        // className="txt"
                        variant="body2"
                        className="text"
                        sx={{
                          fontSize: {
                            md: "0.7rem",
                            xs: "0.6rem",
                            sm: "0.6rem",
                          },
                          fontWeight: "bold",
                        }}
                      >
                        Kandy View Hotel, City Centure, Kandy
                      </Typography>
                      <Popup />
                    </CardContent>
                  </Box>
                </SwiperSlide>
                <SwiperSlide>
                  <Box sx={{ boxShadow: 5, borderRadius: 2, m: "1rem" }}>
                    <Image
                      className="img"
                      src="/images/hotel4.jpg"
                      alt="header"
                      height="215"
                      width="335"
                    />
                    <CardContent sx={{ py: "0.5rem" }}>
                      <Typography
                        // className="txt"
                        variant="body2"
                        className="text"
                        sx={{
                          fontSize: {
                            md: "0.7rem",
                            xs: "0.6rem",
                            sm: "0.6rem",
                          },
                          fontWeight: "bold",
                        }}
                      >
                        Queensburry City Hotel, Nuwara Eliya
                      </Typography>
                      <Popup />
                    </CardContent>
                  </Box>
                </SwiperSlide>
              </Swiper>
            </Box>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default Accommodation;
