import { Box, Container, Grid, Typography, CardContent } from "@mui/material";
import React from "react";
import Image from "next/image";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";

import { Navigation, Pagination, Scrollbar, A11y } from "swiper";
const Adventurous = () => {
  return (
    <Box
      sx={{
        bgcolor: "white",
        pb: 3,
      }}
    >
      <Container>
        <Typography
          sx={{
            textAlign: "left",
            fontFamily: "Chonburi",
            color: "#1B0B7D",

            p: 2,

            fontSize: { md: "1.7rem", xs: "1rem", sm: "1.5rem" },
          }}
        >
          Adventurous
        </Typography>
        {/* <Grid item md={6} xs={12} sm={12}> */}
        <Box>
          <Swiper
            modules={[Navigation, Pagination, Scrollbar, A11y]}
            spaceBetween={0}
            slidesPerView={4}
            navigation
            pagination={{ clickable: true }}
            scrollbar={{ draggable: true }}
            onSwiper={(swiper) => console.log(swiper)}
            onSlideChange={() => console.log("slide change")}
            breakpoints={{
              0: {
                slidesPerView: 2,
                spaceBetween: 10,
              },
              600: {
                slidesPerView: 3,
                spaceBetween: 20,
              },
              900: {
                slidesPerView: 4,
                spaceBetween: 30,
              },
            }}
          >
            <SwiperSlide>
              <Box sx={{ boxShadow: 5, borderRadius: 2, m: "1rem" }}>
                <Image
                  className="img"
                  src="/images/sea.jpg"
                  alt="header"
                  height="215"
                  width="335"
                />
                <CardContent sx={{ py: "0.5rem" }}>
                  <Typography
                    variant="body2"
                    className="text"
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                      fontWeight: "bold",
                    }}
                  >
                    Deep Sea Fishing in SriLanka
                  </Typography>
                </CardContent>
              </Box>
            </SwiperSlide>
            <SwiperSlide>
              <Box sx={{ boxShadow: 5, borderRadius: 2, m: "1rem" }}>
                <Image
                  className="img"
                  src="/images/MountainClimbing.jpg"
                  alt="header"
                  height="215"
                  width="335"
                />
                <CardContent sx={{ py: "0.5rem" }}>
                  <Typography
                    variant="body2"
                    className="text"
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                      fontWeight: "bold",
                    }}
                  >
                    Mountain Climbing in SriLanka
                  </Typography>
                </CardContent>
              </Box>
            </SwiperSlide>
            <SwiperSlide>
              <Box sx={{ boxShadow: 5, borderRadius: 2, m: "1rem" }}>
                <Image
                  className="img"
                  src="/images/Snorkeling.jpg"
                  alt="header"
                  height="215"
                  width="335"
                />
                <CardContent sx={{ py: "0.5rem" }}>
                  <Typography
                    variant="body2"
                    className="text"
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                      fontWeight: "bold",
                    }}
                  >
                    Snorkeling in Maldives
                  </Typography>
                </CardContent>
              </Box>
            </SwiperSlide>
            <SwiperSlide>
              <Box sx={{ boxShadow: 5, borderRadius: 2, m: "1rem" }}>
                <Image
                  className="img"
                  src="/images/Parasailing.jpg"
                  alt="header"
                  height="215"
                  width="335"
                />
                <CardContent sx={{ py: "0.5rem" }}>
                  <Typography
                    variant="body2"
                    className="text"
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                      fontWeight: "bold",
                    }}
                  >
                    Parasailing in Maldives
                  </Typography>
                </CardContent>
              </Box>
            </SwiperSlide>
            <SwiperSlide>
              <Box sx={{ boxShadow: 5, borderRadius: 2, m: "1rem" }}>
                <Image
                  className="img"
                  src="/images/Water-sport.jpg"
                  alt="header"
                  height="215"
                  width="335"
                />
                <CardContent sx={{ py: "0.5rem" }}>
                  <Typography
                    variant="body2"
                    className="text"
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                      fontWeight: "bold",
                    }}
                  >
                    Water Sports Combo In Maldives
                  </Typography>
                </CardContent>
              </Box>
            </SwiperSlide>
            <SwiperSlide>
              <Box sx={{ boxShadow: 5, borderRadius: 2, m: "1rem" }}>
                <Image
                  className="img"
                  src="/images/whaleWatching.jpg"
                  alt="header"
                  height="215"
                  width="335"
                />
                <CardContent sx={{ py: "0.5rem" }}>
                  <Typography
                    variant="body2"
                    className="text"
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                      fontWeight: "bold",
                    }}
                  >
                    Whale watching in SriLanka
                  </Typography>
                </CardContent>
              </Box>
            </SwiperSlide>
          </Swiper>
        </Box>
        {/* </Grid> */}
      </Container>
    </Box>
  );
};

export default Adventurous;
