import React, { useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";

// import "./styles.css";

// import required modules
// import { Pagination } from "swiper";
import Image from "next/image";
import { Box, Container } from "@mui/material";
import { Navigation, Pagination, Scrollbar, A11y } from "swiper";
export default function App() {
  return (
    <Container>
      <Swiper
        // install Swiper modules
        modules={[Navigation, Pagination, Scrollbar, A11y]}
        spaceBetween={50}
        slidesPerView={3}
        navigation
        pagination={{ clickable: true }}
        scrollbar={{ draggable: true }}
        onSwiper={(swiper) => console.log(swiper)}
        onSlideChange={() => console.log("slide change")}
      >
        <SwiperSlide>
          {" "}
          <Image
            className="img"
            src="/images/hotel1.jpg"
            alt="header"
            height="215"
            width="335"
          />
        </SwiperSlide>
        <SwiperSlide>
          {" "}
          <Image
            className="img"
            src="/images/hotel1.jpg"
            alt="header"
            height="215"
            width="335"
          />
        </SwiperSlide>
        <SwiperSlide>
          {" "}
          <Image
            className="img"
            src="/images/hotel1.jpg"
            alt="header"
            height="215"
            width="335"
          />
        </SwiperSlide>
        <SwiperSlide>
          {" "}
          <Image
            className="img"
            src="/images/hotel1.jpg"
            alt="header"
            height="215"
            width="335"
          />
        </SwiperSlide>
      </Swiper>
    </Container>
  );
}
