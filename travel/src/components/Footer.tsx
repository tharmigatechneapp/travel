import { Box, Container, Grid, Typography, Stack, Link } from "@mui/material";
import YouTubeIcon from "@mui/icons-material/YouTube";
import FacebookIcon from "@mui/icons-material/Facebook";
import TwitterIcon from "@mui/icons-material/Twitter";
import InstagramIcon from "@mui/icons-material/Instagram";
import { textAlign } from "@mui/system";
const Footer = () => {
  return (
    <Box bgcolor="#bdd7f9" color="black" sx={{ p: 3 }}>
      <Container>
        <Box
          sx={{ flexGrow: 1, py: 1 }}
          display="flex"
          justifyContent={"center"}
          alignItems="space-evenly"
        >
          <Grid container sx={{ dispaly: "flex" }}>
            <Grid item xs={6} sm={3}>
              <Stack spacing={1}>
                <Typography
                  sx={{
                    fontSize: { xs: "0.6rem", sm: "0.8rem", md: "0.9rem" },
                  }}
                  fontFamily={"Open Sans"}
                  fontWeight={"bold"}
                >
                  Customer Support
                </Typography>
                <Stack
                  spacing={0.5}
                  display="flex"
                  justifyContent={"space-evenly"}
                  alignItems="flex-start"
                >
                  <Typography
                    fontFamily={"Open Sans"}
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                    }}
                  >
                    Contact us
                  </Typography>
                  <Typography
                    fontFamily={"Open Sans"}
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                    }}
                  >
                    Service
                  </Typography>
                  <Typography
                    fontFamily={"Open Sans"}
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                    }}
                  >
                    Feedback
                  </Typography>
                </Stack>
                <Box
                  sx={{
                    "& > :not(style)": {
                      mr: 0.5,
                      display: "row",
                      alignItems: "left",

                      // height: { xs: "0.6rem", sm: "0.6rem", md: "1.5rem" },
                      // width: { xs: "0.6rem", sm: "0.6rem", md: "1.5rem" },
                    },
                  }}
                >
                  <YouTubeIcon
                    sx={{
                      height: { xs: "0.6rem", sm: "1.3rem", md: "1.5rem" },
                      width: { xs: "0.6rem", sm: "1.3rem", md: "1.5rem" },
                    }}
                  />

                  <FacebookIcon
                    sx={{
                      height: { xs: "0.6rem", sm: "1.3rem", md: "1.5rem" },
                      width: { xs: "0.6rem", sm: "1.3rem", md: "1.5rem" },
                    }}
                  />
                  <TwitterIcon
                    sx={{
                      height: { xs: "0.6rem", sm: "1.3rem", md: "1.5rem" },
                      width: { xs: "0.6rem", sm: "1.3rem", md: "1.5rem" },
                    }}
                  />
                  <InstagramIcon
                    sx={{
                      height: { xs: "0.6rem", sm: "1.3rem", md: "1.5rem" },
                      width: { xs: "0.6rem", sm: "1.3rem", md: "1.5rem" },
                    }}
                  />
                </Box>
              </Stack>
            </Grid>

            <Grid item xs={6} sm={3}>
              <Stack spacing={1}>
                <Typography
                  sx={{
                    fontSize: { xs: "0.6rem", sm: "0.8rem", md: "0.9rem" },
                  }}
                  fontFamily={"Open Sans"}
                  fontWeight={"bold"}
                >
                  Quick Links
                </Typography>
                <Stack
                  spacing={0.5}
                  sx={{
                    fontSize: { md: "0.7rem", xs: "0.7rem", sm: "0.8rem" },
                  }}
                >
                  <Typography
                    fontFamily={"Open Sans"}
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                    }}
                  >
                    Hotels
                  </Typography>
                  <Typography
                    fontFamily={"Open Sans"}
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                    }}
                  >
                    Flights
                  </Typography>
                  <Typography
                    fontFamily={"Open Sans"}
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                    }}
                  >
                    Destination
                  </Typography>
                  <Typography
                    fontFamily={"Open Sans"}
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                    }}
                  >
                    Deals
                  </Typography>
                </Stack>
              </Stack>
            </Grid>
            <Grid item xs={6} sm={3}>
              <Stack spacing={1}>
                <Typography
                  sx={{
                    fontSize: { xs: "0.6rem", sm: "0.8rem", md: "0.9rem" },
                  }}
                  fontFamily={"Open Sans"}
                  fontWeight={"bold"}
                >
                  Help Center
                </Typography>
                <Stack spacing={0.5}>
                  <Typography
                    fontFamily={"Open Sans"}
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                    }}
                  >
                    FQS
                  </Typography>
                  <Typography
                    fontFamily={"Open Sans"}
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                    }}
                  >
                    Privacy policy
                  </Typography>
                  <Typography
                    fontFamily={"Open Sans"}
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                    }}
                  >
                    Cookie policy
                  </Typography>
                  <Typography
                    fontFamily={"Open Sans"}
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                    }}
                  >
                    Term of use
                  </Typography>
                </Stack>
              </Stack>
            </Grid>
            <Grid
              item
              xs={3}
              sm={3}
              display="flex"
              justifyContent={"flex-start"}
              alignItems="center"
            >
              <Stack spacing={1}>
                <Typography
                  sx={{
                    fontSize: { xs: "0.6rem", sm: "0.8rem", md: "0.9rem" },
                  }}
                  fontFamily={"Open Sans"}
                  fontWeight={"bold"}
                >
                  Get to know
                </Typography>
                <Stack spacing={0.5}>
                  <Typography
                    fontFamily={"Open Sans"}
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                    }}
                  >
                    About us
                  </Typography>
                  <Typography
                    fontFamily={"Open Sans"}
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                    }}
                  >
                    Room
                  </Typography>
                  <Typography
                    fontFamily={"Open Sans"}
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                    }}
                  >
                    Privacy
                  </Typography>
                  <Typography
                    fontFamily={"Open Sans"}
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                    }}
                  >
                    Terms of use
                  </Typography>
                  <Typography
                    sx={{
                      fontSize: { xs: "0.6rem", sm: "0.7rem", md: "0.8rem" },
                    }}
                    fontFamily={"Open Sans"}
                  >
                    Blog
                  </Typography>
                </Stack>
              </Stack>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </Box>
  );
};

export default Footer;
