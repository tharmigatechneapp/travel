import React from "react";
import {
  Grid,
  CardContent,
  Typography,
  Box,
  Container,
  Paper,
  Link,
} from "@mui/material";
import { styled } from "@mui/material/styles";
import Image from "next/image";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  // padding: theme.spacing(1),
  textAlign: "center",
  // color: theme.palette.text.secondary,
}));
const Deals = () => {
  return (
    <Box
      sx={{
        bgcolor: "#bdd7f9",
        pb: 4,
      }}
    >
      <Container>
        <Typography
          sx={{
            textAlign: "center",
            fontFamily: "Chonburi",
            color: "#1B0B7D",
            // my: "60px",
            // mt: "30",
            p: 2,

            fontSize: { md: "1.7rem", xs: "1rem", sm: "1.5rem" },
          }}
        >
          Explore Top Deals
        </Typography>

        <Box sx={{ flexGrow: 1, p: 1 }}>
          <Grid container spacing={2}>
            <Grid item xs={6} sm={3} sx={{ pt: 20 }}>
              <Link href="/Deals" sx={{ textDecoration: "none" }}>
                <Item sx={{ boxShadow: 5, borderRadius: 3 }}>
                  <Image
                    className="img"
                    src="/images/maldives.jpg"
                    alt="header"
                    height="295"
                    width="345"
                  />
                  <CardContent sx={{ py: "0.5rem", pb: "7px !important" }}>
                    <Typography
                      className="text"
                      sx={{
                        fontSize: { md: "1.2rem", xs: "0.6rem", sm: "0.7rem" },
                        pb: { xs: 0.5, sm: 1, md: 1.5 },

                        // fontWeight: { xs: "2rem", sm: "3rem" },
                        fontWeight: "bold",
                      }}
                    >
                      Sri Lanka
                    </Typography>
                    <Typography
                      variant="body2"
                      color="text.secondary"
                      className="text"
                      sx={{
                        fontSize: { md: "0.7rem", xs: "0.6rem", sm: "0.6rem" },
                        // wordSpacing: "1px",
                      }}
                    >
                      Discover exotic Sri Lanka:more Relax in a 4-star
                      all-inclusive Maldives resort
                    </Typography>

                    <Box
                      display="flex"
                      justifyContent="space-between"
                      alignItems="center"
                      sx={{ py: 1 }}
                    >
                      <Typography
                        sx={{
                          fontSize: {
                            md: "0.7rem",
                            xs: "0.6rem",
                            sm: "0.8rem",
                          },
                        }}
                      >
                        4 Days
                      </Typography>
                      <Typography
                        sx={{
                          color: "red",
                          fontSize: {
                            md: "0.7rem",
                            xs: "0.5rem",
                            sm: "0.6rem",
                          },
                        }}
                      >
                        from
                        <br />
                        <Typography
                          sx={{
                            fontSize: {
                              md: "0.7rem",
                              xs: "0.7rem",
                              sm: "0.8rem",
                            },
                            lineHeight: {
                              xs: "1rem",
                              sm: "1rem",
                              md: "1.2rem",
                            },
                          }}
                        >
                          $2434
                        </Typography>
                      </Typography>
                    </Box>
                  </CardContent>
                </Item>
              </Link>
            </Grid>

            <Grid item xs={6} sm={3}>
              <Item sx={{ boxShadow: 5, borderRadius: 3 }}>
                <Image
                  className="img"
                  src="/images/india.jpg"
                  alt="header"
                  height="295"
                  width="345"
                />
                <CardContent sx={{ py: "0.5rem", pb: "7px !important" }}>
                  <Typography
                    className="text"
                    sx={{
                      fontSize: { md: "1.2rem", xs: "0.6rem", sm: "0.7rem" },
                      pb: { xs: 0.5, sm: 1, md: 1.5 },
                      // fontWeight: { xs: "2rem", sm: "3rem" },
                      fontWeight: "bold",
                    }}
                  >
                    Incredible India
                  </Typography>
                  <Typography
                    variant="body2"
                    color="text.secondary"
                    className="text"
                    sx={{
                      fontSize: { md: "0.7rem", xs: "0.6rem", sm: "0.6rem" },
                    }}
                  >
                    Dive into India’s colourful culture & history Discover Agra,
                    Jaipur, Delhi & Bharatpur
                  </Typography>

                  <Box
                    display="flex"
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ py: 1 }}
                  >
                    <Typography
                      sx={{
                        fontSize: { md: "0.7rem", xs: "0.6rem", sm: "0.8rem" },
                      }}
                    >
                      4 Days
                    </Typography>
                    <Typography
                      sx={{
                        color: "red",
                        fontSize: { md: "0.7rem", xs: "0.5rem", sm: "0.6rem" },
                        textAlign: "left",
                      }}
                    >
                      from
                      <br />
                      <Typography
                        sx={{
                          fontSize: {
                            md: "0.7rem",
                            xs: "0.7rem",
                            sm: "0.8rem",
                          },
                          lineHeight: { xs: "1rem", sm: "1rem", md: "1.2rem" },
                        }}
                      >
                        $2434
                      </Typography>
                    </Typography>
                  </Box>
                </CardContent>
              </Item>
            </Grid>
            <Grid item xs={6} sm={3}>
              <Item sx={{ boxShadow: 5, borderRadius: 3 }}>
                <Image
                  className="img"
                  src="/images/japan.jpg"
                  alt="header"
                  height="295"
                  width="345"
                />
                <CardContent sx={{ py: "0.5rem", pb: "7px !important" }}>
                  <Typography
                    className="text"
                    sx={{
                      fontSize: { md: "1.2rem", xs: "0.6rem", sm: "0.7rem" },
                      pb: { xs: 0.5, sm: 1, md: 1.5 },
                      fontWeight: "bold",
                      // fontWeight: { xs: "2rem", sm: "3rem" },
                    }}
                  >
                    Japan Discovery
                  </Typography>
                  <Typography
                    variant="body2"
                    color="text.secondary"
                    className="text"
                    sx={{
                      fontSize: { md: "0.7rem", xs: "0.6rem", sm: "0.6rem" },
                    }}
                  >
                    Uncover the natural wonders & rich culture of Japan Visit Mt
                    Fuji, Tokyo, Kyoto, Osaka, Nara & more
                  </Typography>

                  <Box
                    display="flex"
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ py: 1 }}
                  >
                    <Typography
                      sx={{
                        fontSize: { md: "0.7rem", xs: "0.6rem", sm: "0.8rem" },
                      }}
                    >
                      4 Days
                    </Typography>
                    <Typography
                      sx={{
                        color: "red",
                        fontSize: { md: "0.7rem", xs: "0.5rem", sm: "0.6rem" },
                        textAlign: "left",
                      }}
                    >
                      from
                      <br />
                      <Typography
                        sx={{
                          fontSize: {
                            md: "0.7rem",
                            xs: "0.7rem",
                            sm: "0.8rem",
                          },
                          lineHeight: { xs: "1rem", sm: "1rem", md: "1.2rem" },
                        }}
                      >
                        $2434
                      </Typography>
                    </Typography>
                  </Box>
                </CardContent>
              </Item>
            </Grid>
            <Grid item xs={6} sm={3}>
              <Item sx={{ boxShadow: 5, borderRadius: 3 }}>
                <Image
                  className="img"
                  src="/images/uk.jpg"
                  alt="header"
                  height="295"
                  width="345"
                />
                <CardContent sx={{ py: "0.5rem", pb: "7px !important" }}>
                  <Typography
                    className="text"
                    sx={{
                      fontSize: { md: "1.2rem", xs: "0.6rem", sm: "0.7rem" },
                      pb: { xs: 0.5, sm: 1, md: 1.5 },
                      // fontWeight: { xs: "2rem", sm: "3rem" },
                      fontWeight: "bold",
                    }}
                  >
                    Taste of UK
                  </Typography>
                  <Typography
                    variant="body2"
                    color="text.secondary"
                    className="text"
                    sx={{
                      fontSize: { md: "0.7rem", xs: "0.6rem", sm: "0.6rem" },
                    }}
                  >
                    Journey through 6 unforgettable countries in one trip Visit
                    Paris, London, & more
                  </Typography>

                  <Box
                    display="flex"
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ py: 1 }}
                  >
                    <Typography
                      sx={{
                        fontSize: { md: "0.7rem", xs: "0.6rem", sm: "0.8rem" },
                        textAlign: "left",
                      }}
                    >
                      4 Days
                    </Typography>
                    <Typography
                      sx={{
                        color: "red",
                        fontSize: { md: "0.7rem", xs: "0.5rem", sm: "0.6rem" },
                        textAlign: "left",
                      }}
                    >
                      from
                      <br />
                      <Typography
                        sx={{
                          fontSize: {
                            md: "0.7rem",
                            xs: "0.7rem",
                            sm: "0.8rem",
                          },
                          lineHeight: { xs: "1rem", sm: "1rem", md: "1.2rem" },
                          textAlign: "left",
                        }}
                      >
                        $2434
                      </Typography>
                    </Typography>
                  </Box>
                </CardContent>
              </Item>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </Box>
  );
};

export default Deals;
